#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/stat.h>
#include <dirent.h>

#include <bemenu.h>


void cd(struct bm_menu *m, const char *path)
{
	printf("%s\n", path);
	struct stat st;
	stat(path, &st);
	if(S_ISDIR(st.st_mode)) {
		printf("This is a directory!\n");
		bm_menu_free_items(m);
		chdir(path);
		DIR *dp = opendir(path);

		bm_menu_set_title(m, path);


		for(struct dirent *iter = readdir(dp);iter;iter = readdir(dp)) {
			if(strcmp(iter->d_name, ".") == 0) continue;
			struct bm_item *i = bm_item_new(iter->d_name);
			bm_menu_add_item(m, i);
		}
		closedir(dp);
		//bm_menu_set_filter(m, NULL);
		bm_menu_set_highlighted_index(m, 1);

	} else {
		printf("This is not a directory\n");
		if(fork() == 0 ) {
			execlp("xdg-open", "xdg-open", path, (char *)NULL);
		} else return;
	}
}

int main(int argc, char *argv[])
{
	struct bm_menu *m;
	unsigned int unicode;
	enum bm_key key;
	enum bm_run_result status = BM_RUN_RESULT_RUNNING;	
	char title[128];
	DIR *dp;

	bm_init();
	m = bm_menu_new(NULL);

	bm_menu_set_highlighted_index(m, 1);
	bm_menu_grab_keyboard(m, true);
	bm_menu_set_panel_overlap(m, false);

	if(argc > 1) {
		realpath(argv[1], title);
		chdir(title);
	} else { 
		realpath(".", title);
	}

	dp = opendir(title);
	
	bm_menu_set_title(m, title);
	

	for(struct dirent *iter = readdir(dp);iter;iter = readdir(dp)) {
		//printf("%s\n", iter->d_name);
		struct bm_item *i = bm_item_new(iter->d_name);
		bm_menu_add_item(m, i);
	}
	closedir(dp);



	do {
		do {
			bm_menu_render(m);
			key = bm_menu_poll_key(m, &unicode);
		} while ((status = bm_menu_run_with_key(m, key, unicode)) == BM_RUN_RESULT_RUNNING);

		unsigned int count;
		struct bm_item **items;
		if(status == BM_RUN_RESULT_SELECTED) items = bm_menu_get_selected_items(m, &count);
		for(int i=0;i<count;i++) { 
			const char *iter = bm_item_get_text(items[i]);
			//printf("%s\n", iter);
			realpath(iter, title);
			cd(m, title);
		}
	} while(status != BM_RUN_RESULT_CANCEL);


	bm_menu_free_items(m);
	bm_menu_free(m);
	
	return 0;
}
